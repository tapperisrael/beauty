// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

/*
function handleOpenURL(url) {
  setTimeout(function() {
//	alert (url);
//var hash = url.substring(url.indexOf('//')+1);
//alert(hash);
  var path = url.slice(7) 
  alert (path);
  window.localStorage.setItem("externalUrl", path); 
  
  }, 0);
}
*/

angular.module('starter', ['ionic', 'starter.controllers','starter.factories','ngStorage','ngCordova','google.places', 'pascalprecht.translate'])

.run(function($ionicPlatform,$rootScope,$http,$localStorage, $window,$state,$ionicHistory,$translate,$ionicPopup) {

	$rootScope.DefaultLanguage = 'he';
		
	$rootScope.searchParams = 
	{
	  "area" : "",
	  "type" : "",
	  "catagory" : "",
	  "day" : "",
	  "time" : ""
	}
   $rootScope.Host = 'http://tapper.co.il/mami_app/php/';
   $rootScope.laravelHost = "http://tapper.co.il/mami_app/laravel/public/";
   $rootScope.currState = $state;
   $rootScope.State = '';
   $rootScope.SearchJson = [];
   $rootScope.SearchAppointment = [];
   $rootScope.sapakSubcat = [];
   $rootScope.CouponsArray = [];
   $rootScope.NavigateMainSearch = '';
   $rootScope.MainSearchArray = [];
   $rootScope.pushRedirect = '';
   $rootScope.ClientAppointments = [];
   $rootScope.selectedCategory = '';
   $rootScope.SelectedSubCategoryName = [];
   $rootScope.lanDirction = 'rtl'
   
   
	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $rootScope.State = newValue;
    });  

	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		if($rootScope.State == 'app.main') 
		{
			
		   var confirmPopup = $ionicPopup.confirm({
			 title: '<p dir="'+$rootScope.lanDirction+'">'+$translate.instant('exitapptextctrl')+'</p>',
			 template: '',
			 cancelText: $translate.instant('canceltextctrl'), 
			 okText: $translate.instant('oktextctrl')		 
		   });
		   confirmPopup.then(function(res) {
			if(res) 
				navigator.app.exitApp();
			else
				event.preventDefault();

		   });	
		}
		else
		{
			 $ionicHistory.goBack();
		}
	},100);

	
	//get coupons
	$http.get($rootScope.Host+'/get_coupons.php')
	.success(function(data, status, headers, config)
	{
		$rootScope.CouponsArray = data;
		//console.log(data)
	})
	.error(function(data, status, headers, config)
	{

	});
			
/*	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	$http.post($rootScope.Host+'/allSuppliers.php',send_params)
	.success(function(data, status, headers, config)
	{
		$rootScope.SearchJson = data;
				
	})
	.error(function(data, status, headers, config)
	{
	
	});*/


   
  $ionicPlatform.ready(function() {
	  



/* 
	if(typeof navigator.globalization !== "undefined") {
		navigator.globalization.getPreferredLanguage(function(language) {
			$translate.use((language.value).split("-")[0]).then(function(data) {
				console.log("SUCCESS -> " + data);
			}, function(error) {
				console.log("ERROR -> " + error);
			});
		}, null);
	}
*/



	
  // push


  var notificationOpenedCallback = function(jsonData) {
	  //alert (JSON.stringify(jsonData));
	  //alert (jsonData.additionalData.type);
	  if (jsonData.additionalData.type == "newappoinment")
	  {
		  if (!jsonData.isActive)
		  {
			  $rootScope.pushRedirect = jsonData.additionalData.supplierid;
			  
				if ($rootScope.pushRedirect)
				{
					window.location ="#/app/ratings/"+$rootScope.pushRedirect;
					$rootScope.pushRedirect = "";
				}			  
		  }
	  }
	  
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal.init("1da8356f-9711-4c7c-bf0d-d300eb445ac1",
                                 {googleProjectNumber: "595323574268"},
                                 notificationOpenedCallback);

  window.plugins.OneSignal.getIds(function(ids) {
	
  $rootScope.pushId = ids.userId;

  //alert ($rootScope.pushId)
  
  
  
  
  
  //if ($rootScope.pushId)
  //{
//	  if ($localStorage.userid =="")
//	  {
	
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"pushId" : $rootScope.pushId
				
			}
	

			$http.post($rootScope.Host+'/save_visitor.php',send_params)
			.success(function(data, status, headers, config)
			{

				//$scope.Appoinments = {};
			})
			.error(function(data, status, headers, config)
			{

			});			  

				
			//console.log('getIds: ' + JSON.stringify(ids));
});

								 
  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(false);



	
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
  
  
  });
})





.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider,$translateProvider) {
	
  // configures staticFilesLoader
  $translateProvider.useStaticFilesLoader({
    prefix: 'js/data/locale-',
    suffix: '.json'
  });
  // load 'en' table on startup
  $translateProvider.preferredLanguage('he');
  $translateProvider.fallbackLanguage('he');

  
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

  .state('app', 
  {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.main', {
    url: '/main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
		controller: 'MainCtrl'
      }
    }
  })

  .state('app.search', {
    url: '/search/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
		controller: 'SearchCtrl'
      }
    }
  })
  
  .state('app.list', {
    url: '/list/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/list.html',
		controller: 'ListCtrl'
      }
    }
  })
  
  .state('app.listmain', {
    url: '/listmain/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/listmain.html',
		controller: 'ListCtrl'
      }
    }
  })

  
  .state('app.details', {
    url: '/details/:CatIndex/:IndexArray/:ItemIndex',
    views: {
      'menuContent': {
        templateUrl: 'templates/details.html',
		controller: 'detailstCtrl'
      }
    }
  })
  
  .state('app.searchappoint', {
    url: '/searchappoint/:CatIndex/:IndexArray',
    views: {
      'menuContent': {
        templateUrl: 'templates/searchappoint.html',
		controller: 'searchAppoinmentCtrl'
      }
    }
  })  
   .state('app.appointments', {
    url: '/appointments',
    views: {
      'menuContent': {
        templateUrl: 'templates/appointments.html',
		controller: 'AppointmentsCtrl'
      }
    }
  })
   .state('app.supplier', {
    url: '/supplier',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplier.html',
		controller: 'SupplierCtrl'
      }
    }
  })
  
   .state('app.supplierLogin', {
    url: '/supplierLogin',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplier_login.html',
		controller: 'SupplierLoginCtrl'
      }
    }
  })

   .state('app.supplierForgot', {
    url: '/supplierForgot',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplier_forgot.html',
		controller: 'SupplierForgotCtrl'
      }
    }
  })  
  
   .state('app.login', {
    url: '/login/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
		controller: 'LoginRegisterCtrl'
      }
    }
  })
  
   .state('app.forgotLogin', {
    url: '/forgotLogin',
    views: {
      'menuContent': {
        templateUrl: 'templates/forgot_login.html',
		controller: 'ForgotLoginCtrl'
      }
    }
  })

  
   .state('app.register', {
    url: '/register/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
		controller: 'LoginRegisterCtrl'
      }
    }
  })  
    .state('app.suppliermain', {
    url: '/suppliermain',
    views: {
      'menuContent': {
        templateUrl: 'templates/suppliermain.html',
		controller: 'SupplierMain'
      }
    }
  })
  
    .state('app.supplierpersonal', {
    url: '/supplierpersonal',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplierpersonal.html',
		controller: 'SupplierPersonal'
      }
    }
  })
  
     .state('app.supplierappoint', {
    url: '/supplierappoint',
    views: {
      'menuContent': {
        templateUrl: 'templates/supplierappoint.html',
		controller: 'SupplierAppointments'
      }
    }
  })
  
  
     .state('app.editappoint', {
    url: '/editappoint/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/editappoint.html',
		controller: 'EditAppointCtrl'
      }
    }
  })
  
     .state('app.editfreeapp', {
    url: '/editfreeapp/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/editfreeapp.html',
		controller: 'EditFreeAppCtrl'
      }
    }
  })
  
  
  
    .state('app.addappoint', {
    url: '/addappoint',
    views: {
      'menuContent': {
        templateUrl: 'templates/addappoint.html',
		controller: 'SupplierAddAppoinmenet'
      }
    }
  })
  /*
    .state('app.orderedappoinment', {
    url: '/orderedappoinment',
    views: {
      'menuContent': {
        templateUrl: 'templates/orderedappoinment.html',
		controller: 'SupplierOrderedAppoinmenets'
      }
    }
  })
  */
  
   .state('app.coupons', {
    url: '/coupons',
    views: {
      'menuContent': {
        templateUrl: 'templates/coupons.html',
		controller: 'CouponsCtrl'
      }
    }
  })

   .state('app.updateinfo', {
    url: '/updateinfo',
    views: {
      'menuContent': {
        templateUrl: 'templates/updateinfo.html',
		controller: 'UpdateInfoCtrl'
      }
    }
  })

  
   
  .state('app.coupondetails', {
    url: '/coupondetails/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/coupondetails.html',
		controller: 'CouponsDetailsCtrl'
      }
    }
  })
  
  .state('app.ratings', {
    url: '/ratings/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/ratings.html',
		controller: 'RatingsCtrl'
      }
    }
  });

  
  


  
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
})

.run(['$state', '$window',
    function($state, $window) {
        $window.addEventListener('LaunchUrl', function(event) {
		  var path = event.detail.url.slice(7) 
		  window.location ="#/app/ratings/"+path;
        });
    }
]);

function handleOpenURL(url) {
    setTimeout( function() {
        var event = new CustomEvent('LaunchUrl', {detail: {'url': url}});
        window.dispatchEvent(event);
    }, 0);
}
